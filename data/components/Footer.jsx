export default {
    logo : "logo.png",
    col1:[
        {
            link:"/",
            text:"Home",
        },
        {
            link:"/about-us",
            text:"About us",
        },
        {
            link:"/contact",
            text:"Contact",
        },
        {
            link:"/service",
            text:"Services",
        },
    ],
    col2:[
        {
            link:"/",
            text:"Instagram",
        },
        {
            link:"/",
            text:"Twitter",
        },
    ],
}