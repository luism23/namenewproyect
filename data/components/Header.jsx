export default {
    logo : "logo.png",
    nav:[
        {
            link:"/",
            text:"Home",
        },
        {
            link:"/about-us",
            text:"About us",
        },
        {
            link:"/contact",
            text:"Contact",
        },
        {
            link:"/service",
            text:"Services",
        },
    ],
}