export default {
    acount: {
        img1: "img1.png",
        img2: "img2.png",
        title: "Find 3D Objects, Mockups and Illustrations here.",
        text1: "English(UK)",
        img3: "flecha.png",
        title2: "Create Account",
        title3:"- OR -",
        registerRs: [
            {
                link: "/",
                linkText: "Sign up with google",
                img: "google.png"
            },
            {
                link: "/",
                linkText: "Sign up with Facebook",
                img: "facebook.png"
            },
        ],
        text2:"Already have an account?",
        link:"/",
        linkText:"Login",
    }
}