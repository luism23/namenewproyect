import Link from "next/link"

const RegisterRs = ({ link = "", linkText = "", img = "" }) => {
    return (
        <>
            <div className="m-b-50 flex flex-align-center border-radius-20 border-style-solid border-1 border-gray-1 p-v-5 p-h-10">
                <img className="" src={`/image/${img}`} alt="" />
                <Link href={link}>
                    <a className="font-poppins font-21 font-w-500 color-gray-1">
                        {linkText}
                    </a>
                </Link>

            </div>
        </>
    )
}

const Index = ({ img1 = "", img2 = "", title = "", text1 = "", img3 = "", title2 = "", registerRs = [], title3 = "", text2 = "", linkText = "", link = "", textTitle = "" }) => {
    return (
        <>
            <div className="bg-blue-1 p-v-50">
                <div className="container p-h-15 flex">
                    <div className="flex-12 flex-md-5 ">
                        <div className="">
                            <div className="">
                                <img className="m-b-12 m-r-15 " src={`/image/${img1}`} alt="" />
                            </div>
                            <div>
                                <h1 className="font-poppins font-30 font-w-600 color-white m-b-104">
                                    {title}
                                </h1>
                                <div className="">
                                    <img className="d-none d-md-block" src={`/image/${img2}`} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex-12 flex-md-7 p-26 bg-white border-radius-20 ">
                        <div className="flex m-b-21 flex-justify-right">
                            <h4 className="font-poppins font-18 font-w-600 color-gray-1 ">
                                {text1}
                            </h4>
                            <img className="" src={`/image/${img3}`} alt="" />
                        </div>
                        <div className="m-b-49 text-center">
                            <h1 className="font-poppins font-48 font-w-700 color-gray-1">
                                {title2}
                            </h1>
                        </div>
                        <div className="flex flex-justify-center ">
                            {
                                registerRs.map((e, i) => {
                                    return (
                                        <RegisterRs
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                        <div className="m-b-50 flex-justify-center flex color-gray-1 font-poppins font-w-500 text-center font-20">
                            <h2 className="">
                                {title3}
                            </h2>
                        </div>

                        <div className="text-center">
                                <input className="border-radius-20 color-gray-1 font-poppins font-25 m-b-16 login-input border-gray-1 border-1 border-style-solid p-v-5 p-l-12" type="text" placeholder="Full name" />
                                <input className="border-radius-20 color-gray-1 font-poppins font-25  m-b-16 login-input border-gray-1 border-1 border-style-solid p-v-5 p-l-12" type="email" placeholder="Email Address" />
                                <input className="border-radius-20 color-gray-1 font-poppins font-25  m-b-16 login-input border-gray-1 border-1 border-style-solid p-v-5 p-l-12" type="text" placeholder="Password" />
                        </div>
                        <div className="m-v-46" >
                            <div className="text-center">
                                <input className="bg-blue-1 border-radius-50 border-0 p-v-15 p-h-142 color-white font-poppins font-w-700 font-30" type="submit" value="Create Account" />
                            </div>

                        </div>
                        <div className="flex flex-align-center flex-justify-center">
                            <p className="m-r-11 font-26 font-w-500 font-poppins color-gray-1">
                                {text2}

                            </p>
                            <Link href={link}>
                                <a className="font-26 font-w-500 font-poppins color-blue-1">
                                    {linkText}
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>


            </div>
        </>
    )
}
export default Index