const log = (name,data,color="white") => {
    if (process?.env?.MODE === "DEV") {
        console.log(`%c [${name.toLocaleUpperCase()}]`, `color:${color};`, data);
    }
}

export default log